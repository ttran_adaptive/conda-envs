# Conda Environment Collection

## Introduction

This repo aims to provide a collection of ready-to-use yaml files for conda environments.
Major packages are listed here for a quick understanding of what are included in each environment.

- [analytics-3.6.yml](yaml_files/analytics-3.6.yml): 
  - Python-3.6 and python data analytics packages (pandas, numpy, scipy, pyarrow, etc.)
  - adaptive-python-helpers v3.1
  - R 4.1 and R data analytics packages (including tidyverse and arrow)

## Usage

* Navigate to [yaml_files](yaml_files) and download the environment file of your choice.
* On your machine with conda/mamba pre-installed, create the environment from the file:

```shell
mamba env create -f env_file_name.yml
```

* Activate and use:

```shell
mamba activate env_of_your_choice
```
